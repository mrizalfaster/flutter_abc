import 'package:flutter/material.dart';

class TambahPage extends StatefulWidget {
  const TambahPage({Key? key}) : super(key: key);

  @override
  _TambahPageState createState() => _TambahPageState();
}

class _TambahPageState extends State<TambahPage> {
  List<String> items = [
    'Butter Cookies Vanila',
    'Cookies wafer coklat',
    'Kue Garpu'
  ];

  void addItem() {
    setState(() {
      items.add('New Item');
    });
  }

  void editItem(int index) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        String editedItem = items[index];
        return AlertDialog(
          title: Text('Edit Item', style: TextStyle(fontSize: 20)),
          content: TextField(
            onChanged: (value) {
              editedItem = value;
            },
            decoration: InputDecoration(
              labelText: 'Item',
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                setState(() {
                  items[index] = editedItem;
                });
                Navigator.pop(context);
              },
              child: Text('Simpan', style: TextStyle(fontSize: 18)),
            ),
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('Batal', style: TextStyle(fontSize: 18)),
            ),
          ],
        );
      },
    );
  }

  void removeItem(int index) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Konfirmasi', style: TextStyle(fontSize: 20)),
          content: Text('Apakah Anda yakin ingin menghapus item ini?',
              style: TextStyle(fontSize: 18)),
          actions: [
            TextButton(
              onPressed: () {
                setState(() {
                  items.removeAt(index);
                });
                Navigator.pop(context);
              },
              child: Text('Ya', style: TextStyle(fontSize: 18)),
            ),
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('Tidak', style: TextStyle(fontSize: 18)),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tambah Page', style: TextStyle(fontSize: 24)),
      ),
      body: ListView.builder(
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text(items[index], style: TextStyle(fontSize: 20)),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () => editItem(index),
                ),
                IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () => removeItem(index),
                ),
              ],
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: addItem,
        child: Icon(Icons.add),
      ),
    );
  }
}
