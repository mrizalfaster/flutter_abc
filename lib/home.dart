import 'package:flutter/material.dart';
import 'package:flutter_abc/login.dart'; // Import LoginPage
import 'package:flutter_abc/resep.dart';
import 'package:flutter_abc/tambah.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  void navigateToPenjaPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ResepPage()),
    );
  }

  void navigateToTambahPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => TambahPage()),
    );
  }

  void navigateToLoginPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("HomePage"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/reseprahasia.png',
            ),
            SizedBox(height: 20),
            Text(
              "Pilihlah Resep",
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 20),
            ElevatedButton.icon(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                  Colors.orange,
                ),
                padding: MaterialStateProperty.all(
                  EdgeInsets.symmetric(
                    vertical: 12,
                    horizontal: 24,
                  ),
                ),
              ),
              onPressed: () => navigateToPenjaPage(context),
              icon: Icon(Icons.logout_outlined),
              label: Text(
                "RESEP RAHASIA",
                style: TextStyle(fontSize: 20),
              ),
            ),
            SizedBox(height: 20),
            ElevatedButton.icon(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                  Color.fromARGB(255, 255, 139, 97),
                ),
                padding: MaterialStateProperty.all(
                  EdgeInsets.symmetric(
                    vertical: 12,
                    horizontal: 24,
                  ),
                ),
              ),
              onPressed: () => navigateToTambahPage(context),
              icon: Icon(Icons.add),
              label: Text(
                "Tambah Resep",
                style: TextStyle(fontSize: 20),
              ),
            ),
            SizedBox(height: 20),
            IconButton(
              icon: Icon(Icons.exit_to_app),
              onPressed: () => navigateToLoginPage(context),
              tooltip: 'Keluar',
            ),
          ],
        ),
      ),
    );
  }
}
