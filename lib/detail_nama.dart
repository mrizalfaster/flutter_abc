class resep {
  String name, htm, tutorial, image;

  resep(
      {required this.name,
      required this.htm,
      required this.tutorial,
      required this.image});
}

List<resep> dataResep = [
  resep(
    name: 'Butter Cookies',
    htm: '45K',
    tutorial:
        '1. Mixer butter, margarine, dan gula halus selama sekitar 1 menit.\n'
        '   Masukkan kuning telur, mixer sebentar hingga rata, cukupkan.\n'
        '2. Ayak tepung dan baking powder, lalu masukkan ke adonan. Aduk rata menggunakan spatula.\n'
        '3. Terakhir, masukkan nestum dan aduk rata.\n'
        '4. Siapkan loyang anti lengket atau alasi loyang dengan baking paper. Bentuk adonan menjadi bulat, lalu pipihkan atau sesuai selera. Beri topping kacang almond.\n'
        '5. Panggang dengan suhu 150 derajat Celcius selama 20 menit.',
    image: 'assets/nestumcookies.jpeg',
  ),
  resep(
    name: 'Cookies Wafer Keju',
    htm: '45K',
    tutorial: '1. Kocok 250 gr mentega dan 100 gr gula halus hingga rata.\n'
        '   Masukkan 1 telur utuh dan 1 kuning telur. Kocok selama 3 menit hingga agak pucat.\n'
        '2. Masukkan terigu 350 gr, maizena 30 gr, dan susu bubuk 20 gr. Saring dan aduk dengan spatula.\n'
        '3. Tipiskan adonan, letakkan wafer rasa keju di atasnya, gulung dan padatkan menyelimuti bentuk wafernya.\n'
        '4. Potong-potong sesuai selera. Olesi kuning telur, taburi keju parut, panggang hingga matang dengan api sedang sesuai oven masing-masing.\n',
    image: 'assets/waferkeju.jpg',
  ),
  resep(
    name: 'Kue Garpu',
    htm: '60K',
    tutorial:
        '1. Dalam wadah, campurkan terigu dan mentega. Aduk rata hingga berbentuk butiran pasir.\n'
        '2. Masukkan telur, keju, penyedap rasa, garam, dan merica. Aduk rata.\n'
        '3. Terakhir, masukkan air sedikit demi sedikit dan uleni hingga adonan kalis.\n'
        '4. Ambil sedikit adonan, pipihkan sampai tipis di bagian punggung garpu, lalu gulung. Ulangi hingga adonan habis.\n'
        '5. Panaskan minyak, masukkan kue garpu, kecilkan api, dan angkat ketika sudah kecokelatan. Tiriskan.\n'
        '6. Jika kue garpu sudah dingin, masukkan ke dalam toples.\n',
    image: 'assets/kuegarpu.jpeg',
  ),
  resep(
    name: 'Putri Salju',
    htm: '30K',
    tutorial:
        '1. Campur margarin dan gula halus. Masukkan kuning telur dan pasta pandan. Campur sebentar hingga rata, lalu matikan mixer.\n'
        '2. Masukkan tepung dan susu bubuk yang sudah diayak. Aduk dengan sendok kayu hingga rata.\n'
        '3. Bentuk adonan menjadi bulat seberat kurang lebih 6 gram atau bisa juga dicetak. Oven hingga matang dengan suhu 160 derajat Celcius atau sesuaikan dengan oven.\n'
        '4. Dinginkan, lalu gulingkan di bahan taburan.\n',
    image: 'assets/putrisalju.jpeg',
  ),
  resep(
    name: 'Donat Kentang',
    htm: '60K',
    tutorial:
        '1. Campurkan tepung terigu, susu bubuk, gula, dan ragi. Aduk hingga rata.\n'
        '   Tambahkan kentang halus, masukkan kuning telur, dan tambahkan air sedikit demi sedikit sambil diuleni hingga rata dan setengah kalis.\n'
        '2. Masukkan garam dan mentega. Terus uleni hingga kalis dan elastis. Diamkan selama kurang lebih 20-30 menit. Tutupi dengan serbet lembab agar adonan tidak kering.\n'
        '3. Kempiskan adonan, bagi adonan masing-masing 50 gr dan bulatkan. Diamkan 20-30 menit hingga mengembang.\n'
        '4. Lubangi tengahnya menjadi bentuk donat. Segera goreng hingga kuning kecoklatan.\n'
        '5. Goreng donat dengan api sedang cenderung kecil hingga kecoklatan. Angkat dan tiriskan. Setelah dingin, bisa diberi topping sesuai selera.\n',
    image: 'assets/donatkentang.jpg',
  ),
];
